//
//  NarrationService.swift
//  FIT5120-Hear_The_Sea-Alpha
//
//  Created by Daniel Liu on 22/08/2016.
//  Copyright © 2016 Daniel Liu. All rights reserved.
//

import AVFoundation

// Enumerate used to classify callback sender
enum CallbackSender {
    case Surrounding, Detail, Navigation
}

class NarrationService : NSObject, AVSpeechSynthesizerDelegate {
    
    // Variable to store current callback sender
    var currentCallbackSender: CallbackSender!
    
    // This variables stores the synthesizer which used to convert text to speech
    var synthesizer:AVSpeechSynthesizer!
    
    // The current speaking utterance
    var currentUtterance: AVSpeechUtterance!
    
    // The following variables is used to provide callback feature
    var callbackUtterance: AVSpeechUtterance!
    var callbackInterval: Double!
    var callbackTimer: NSTimer!
    var callbackNeeded: Bool = false
    var callback: (() -> Void)!
    
    // Variable used to specify whether current utterance can be stopped or not
    var unstopableUtterance: AVSpeechUtterance!
    // First require no stop, then when the utterance is speaking, switch the no stop flag to true
    var noStopRequired: Bool = false
    var noStop: Bool = false
    
    // Default settings for the utterance
    // Nora's voice is controlled by these settings
    let defaultPitchMultiplier:Float = 1.57
    let defaultSpeechRate:Float = 0.43
    let defaultLanguage:String = "en-US"
    
    // This method will return a standard utterance which represents a sentect to read
    func getDefaultUtterance(string: String) -> AVSpeechUtterance{
        let utt:AVSpeechUtterance = AVSpeechUtterance(string: string)
        utt.pitchMultiplier = self.defaultPitchMultiplier
        utt.rate = self.defaultSpeechRate
        utt.voice = AVSpeechSynthesisVoice(language: self.defaultLanguage)
        return utt
    }
    
    // Initializer
    override init() {
        super.init()
        synthesizer = AVSpeechSynthesizer()
        synthesizer.delegate = self
    }
    
    // This method will read out the message passed to it
    // The priority of this method is the lowest one
    func normalMessage(string: String) {
        synthesizer.speakUtterance(getDefaultUtterance(string))
    }
    
    // This method is introduced to add multi-language support to Nora
    func normalMessage(string: String, language: String) {
        let utterance = getDefaultUtterance(string)
        utterance.voice = AVSpeechSynthesisVoice(language: language)
        utterance.pitchMultiplier = 1.4
        utterance.rate = 0.44
        synthesizer.speakUtterance(utterance)
    }
    
    // This method will stop the current reading sentence and speak the urgent message and read the original message again
    func urgentMessage(string: String) {
        // If the synthesizer is speaking, store current speaking utterance and read it after the urgent message
        // Special situation, the synthesizer is reading and the caller requests the callback, we put urgent message to be played after urgent message
        if synthesizer.speaking && !callbackNeeded {
            let storedUtterance = getDefaultUtterance(currentUtterance.speechString)
            synthesizer.stopSpeakingAtBoundary(AVSpeechBoundary.Immediate)
            synthesizer.speakUtterance(getDefaultUtterance(string))
            synthesizer.speakUtterance(storedUtterance)
        } else {
            // Speak the urgent message normally
            synthesizer.speakUtterance(getDefaultUtterance(string))
        }
    }
    
    // This function adds an callback after the narration is finished
    func narrateWithCallback(sender: CallbackSender, string: String, callbackInterval: Double, callback: () -> Void) {
        currentCallbackSender = sender
        callbackNeeded = true
        self.callback = callback
        self.callbackInterval = callbackInterval
        callbackUtterance = getDefaultUtterance(string)
        synthesizer.speakUtterance(callbackUtterance)
    }
    
    // This function is used to invalidate the callback timer
    func resetCallback() {
        callbackUtterance = nil
        callbackNeeded = false
        if callbackTimer != nil {
            callbackTimer.invalidate()
            callbackTimer = nil
        }
        callback = nil
        currentCallbackSender = nil
    }
    
    // This method performs the callback provided by the narrateWithCallback call
    func performCallback() {
        let localCallback = self.callback
        resetCallback()
        localCallback()
    }
    
    // This function enables nora to speak something which can not be stopped instead of urgent message
    func narrateUnstopableMessage(string: String) {
        noStopRequired = true
        unstopableUtterance = getDefaultUtterance(string)
        synthesizer.speakUtterance(unstopableUtterance)
    }
    
    // Return current state of Narration Service
    func isReading() -> Bool{
        return synthesizer.speaking
    }
    
    // Return whether we are dealing with callback message
    func isHandlingCallback() -> Bool {
        return callbackTimer != nil && callbackTimer.valid
    }
    
    // Reset function is designed to stop all the current works
    func reset() {
        if !noStop {
            synthesizer.stopSpeakingAtBoundary(AVSpeechBoundary.Immediate)
            resetCallback()
        }
    }
    
    // MARK: - Delegate Functions: Speech Synthesizer Delegate
    
    func speechSynthesizer(synthesizer: AVSpeechSynthesizer, didCancelSpeechUtterance utterance: AVSpeechUtterance) {
        currentUtterance = nil
    }
    
    func speechSynthesizer(synthesizer: AVSpeechSynthesizer, didFinishSpeechUtterance utterance: AVSpeechUtterance) {
        currentUtterance = nil
        
        // Handle the callback needed message
        if callbackNeeded && utterance == callbackUtterance{
            callbackTimer = NSTimer(timeInterval: callbackInterval, target: self, selector: #selector(performCallback), userInfo: nil, repeats: false)
            NSRunLoop.mainRunLoop().addTimer(callbackTimer, forMode: NSRunLoopCommonModes)
        }
        
        // Handle the unstopable message
        if noStop && utterance == unstopableUtterance {
            noStop = false
        }
        
    }
    
    func speechSynthesizer(synthesizer: AVSpeechSynthesizer, didStartSpeechUtterance utterance: AVSpeechUtterance) {
        currentUtterance = utterance
        
        // Handle the no stop request
        if noStopRequired && utterance == unstopableUtterance {
            noStopRequired = false
            noStop = true
        }
    }
    
}
