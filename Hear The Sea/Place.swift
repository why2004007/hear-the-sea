//
//  Place.swift
//  Hear The Sea
//
//  Created by Daniel Liu on 15/09/2016.
//  Copyright © 2016 Daniel Liu. All rights reserved.
//

import Foundation
import CoreLocation

class Place : NSObject {
    
    // The name of the place
    var name: String
    // The category and type of the place
    var category: PlaceCategory
    var type: PlaceType
    // If type is not edge and wall, the object has radius and location
    var radius: Int?
    var location: CLLocation?
    // If type is edge and wall, we do not need radius, and location is replaced by a series of nodes
    var route: [CLLocation]?
    // This variable is used to store detail about the location
    var detail: String
    
    init(aName: String, aCategory: PlaceCategory, aType: PlaceType, aRadius: Int?, aLocation: CLLocation?, aRoute: [CLLocation]?, aDetail: String) {
        self.name = aName
        self.category = aCategory
        self.type = aType
        self.radius = aRadius
        self.location = aLocation
        self.route = aRoute
        self.detail = aDetail
    }
}