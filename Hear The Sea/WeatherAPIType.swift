//
//  WeatherAPIType.swift
//  FIT5120-Hear_The_Sea-Alpha
//
//  Created by Daniel Liu on 23/08/2016.
//  Copyright © 2016 Daniel Liu. All rights reserved.
//

import Foundation
import Gloss

struct UVIndexResponse: Decodable {
    let data : Float!
    init?(json: JSON) {
        data = "data" <~~ json
    }
}

struct Coord: Decodable {
    
    let lat : Float!
    let lon : Float!
    
    init?(json: JSON){
        lat = "lat" <~~ json
        lon = "lon" <~~ json
    }
}

struct City: Decodable {
    
    let coord : Coord!
    let country : String!
    let id : Int!
    let name : String!
    let population : Int!
    
    init?(json: JSON){
        coord = "coord" <~~ json
        country = "country" <~~ json
        id = "id" <~~ json
        name = "name" <~~ json
        population = "population" <~~ json
    }
}

struct Cloud: Decodable {
    
    let all : Int!
    
    init?(json: JSON){
        all = "all" <~~ json
    }
}

struct Main: Decodable {
    
    let grndLevel : Float!
    let humidity : Int!
    let pressure : Float!
    let seaLevel : Float!
    let temp : Float!
    let tempKf : Int!
    let tempMax : Float!
    let tempMin : Float!
    
    init?(json: JSON){
        grndLevel = "grnd_level" <~~ json
        humidity = "humidity" <~~ json
        pressure = "pressure" <~~ json
        seaLevel = "sea_level" <~~ json
        temp = "temp" <~~ json
        tempKf = "temp_kf" <~~ json
        tempMax = "temp_max" <~~ json
        tempMin = "temp_min" <~~ json
    }
}

struct Weather: Decodable {
    
    let descriptionField : String!
    let icon : String!
    let id : Int!
    let main : String!
    
    init?(json: JSON){
        descriptionField = "description" <~~ json
        icon = "icon" <~~ json
        id = "id" <~~ json
        main = "main" <~~ json
    }
}

struct Wind: Decodable {
    
    let deg : Float!
    let speed : Float!
    
    init?(json: JSON){
        deg = "deg" <~~ json
        speed = "speed" <~~ json
    }
}

struct Rain: Decodable {
    
    let volume : Float!
    
    init?(json: JSON) {
        volume = "3h" <~~ json
    }
    
}

struct Snow: Decodable {
    let volume : Float!
    
    init?(json: JSON) {
        volume = "3h" <~~ json
    }
}

struct Forecast: Decodable {
    
    let clouds : Cloud!
    let dt : Int!
    let main : Main!
    let rain : Rain!
    let snow : Snow!
    let weather : [Weather]!
    let wind : Wind!
    
    init?(json: JSON){
        clouds = "clouds" <~~ json
        dt = "dt" <~~ json
        main = "main" <~~ json
        rain = "rain" <~~ json
        snow = "snow" <~~ json
        weather = "weather" <~~ json
        wind = "wind" <~~ json
    }

}

struct WeatherAPIResponse: Decodable {
    
    let city : City!
    let cnt : Int!
    let list : [Forecast]!
    
    init?(json: JSON){
        city = "city" <~~ json
        cnt = "cnt" <~~ json
        list = "list" <~~ json
    }
}