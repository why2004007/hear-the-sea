//
//  AlarmManager.swift
//  FIT5120-Hear_The_Sea-Alpha
//
//  Created by Daniel Liu on 22/08/2016.
//  Copyright © 2016 Daniel Liu. All rights reserved.
//

import UIKit
import CoreLocation

class AlarmService : LocationInformationDelegate, HeadingInformationDelegate {
    
    // Stored location and heading
    var currentLocation: CLLocation!
    var currentHeading: CLHeading!
    
    // Store dangerous locations which will be checked when user is walking
    var dangerousLocations = [Place]()
    
    // NarrationService, used to narrate warnings
    var narrationService: NarrationService
    
    // Check dangerous location peridocally
    var alreadyCheckedDangerousLocation = [NSDictionary]()
    
    // We want the gps signal weak warning to be less frequently
    var lastGpsSignalWeakWarningPlayedTime: NSDate!
    
    // Maintain a reference to AppDelegate
    var appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    // Initialize the class
    init(aNarrationService: NarrationService) {
        narrationService = aNarrationService
        
        // Register myself to receive the location and heading udpates
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.registerLocationInformationDelegate(self)
        appDelegate.registerHeadingInformationDelegate(self)
        
        // Load dangerous locations from settings
        let locationList = appDelegate.settings!["StKildaData"] as! [NSDictionary]
        for location in locationList {
            
            let typeString = location["type"] as! String
            let type = PlaceType(rawValue: typeString)!
            let category = SurroundingsInformationProvider.placeTypeToCategory(type)
            if category == .DangerousLocation {
                if type == .edge || type == .wall {
                    var route = [CLLocation]()
                    for node in location["locationList"] as! [NSDictionary] {
                        let latitude = node["latitude"] as! Double
                        let longitude = node["longitude"] as! Double
                        let realLocation = CLLocation(latitude: latitude, longitude: longitude)
                        route.append(realLocation)
                    }
                    let name = type.rawValue
                    let detail = type.rawValue
                    let place = Place(aName: name, aCategory: category, aType: type, aRadius: nil, aLocation: nil, aRoute: route, aDetail: detail)
                    dangerousLocations.append(place)
                } else {
                    let locationDict = location["location"] as! NSDictionary
                    let latitude = locationDict["latitude"] as! Double
                    let longitude = locationDict["longitude"] as! Double
                    let realLocation = CLLocation(latitude: latitude, longitude: longitude)
                    let radius = location["radius"] as! Int
                    let name = type.rawValue
                    let detail = type.rawValue
                    let place = Place(aName: name, aCategory: category, aType: type, aRadius: radius, aLocation: realLocation, aRoute: nil, aDetail: detail)
                    dangerousLocations.append(place)
                }
            }
        }
    }
    
    // Narrate the dangerous location warning to user
    func processDangerousLocationWarning(dangerousPlace: Place) {
        narrationService.normalMessage("Warning, you are approching \(dangerousPlace.type.rawValue), please be careful.")
    }
    
    // This function will calculate whether user is walking towards the dangerous edge and walls
    func calculateDistanceToRoute(point: CLLocation, route: [CLLocation]) -> Double {
        if currentHeading != nil {
            // First set shortest distance as infinity
            var shortestDistance: Double = Double.infinity
            // Store nearest location
            var nearestLocation: CLLocation! = nil
            // Check all the node in the route to get the shortest distance
            for pointOnRoute in route {
                let distanceToPoint = point.distanceFromLocation(pointOnRoute)
                if shortestDistance > distanceToPoint {
                    nearestLocation = pointOnRoute
                    shortestDistance = distanceToPoint
                }
            }
            return shortestDistance
        } else {
            return Double.infinity
        }
    }
    
    // This function will judge whether user is approaching dangerous locations
    @objc func checkDangerousLocation() {
        var dangerousLocation: Place!
        if currentLocation != nil {
            for dangerousPlace in dangerousLocations {
                if dangerousPlace.type == .edge || dangerousPlace.type == .wall {
                    if calculateDistanceToRoute(currentLocation!, route: dangerousPlace.route!) < 5 {
                        dangerousLocation = dangerousPlace
                        break
                    }
                } else {
                    if dangerousPlace.location!.distanceFromLocation(currentLocation!) < Double(dangerousPlace.radius!) + 2.0 {
                        dangerousLocation = dangerousPlace
                        break
                    }
                }
            }
            
            if dangerousLocation != nil {
                // Perform the warning
                var alreadyWarned = false
                for warnedLocation in alreadyCheckedDangerousLocation {
                    let warnedTime = warnedLocation["time"] as! NSDate
                    if abs(warnedTime.timeIntervalSinceNow) > 60 {
                        alreadyCheckedDangerousLocation.removeAtIndex(alreadyCheckedDangerousLocation.indexOf(warnedLocation)!)
                        continue
                    }
                    let warnedPlace = warnedLocation["location"] as! Place
                    if warnedPlace == dangerousLocation {
                        alreadyWarned = true
                        break
                    }
                }
                if !alreadyWarned {
                    let warnedLocation: NSDictionary = [
                        "location": dangerousLocation,
                        "time": NSDate()
                    ]
                    alreadyCheckedDangerousLocation.append(warnedLocation)
                    processDangerousLocationWarning(dangerousLocation)
                }
            }
            
            return
        } else {
            return
        }
    }
    
    // This function will check whether gps signal is weak or not
    func checkGpsSignal() {
        if currentLocation.horizontalAccuracy > 10 {
            // If last warning is played five minute ago, we play it again.
            if lastGpsSignalWeakWarningPlayedTime == nil || abs(lastGpsSignalWeakWarningPlayedTime!.timeIntervalSinceNow) > 300 {
                narrationService.normalMessage("Warning, gps signal is weak, current location could be inaccurate.")
                lastGpsSignalWeakWarningPlayedTime = NSDate()
            }
        }
    }
    
    
    // MARK: - Delegate Functions: Location Information Delegate & Heading Information Delegate
    func locationUpdated(location: CLLocation) {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        if currentLocation == nil {
            currentLocation = location
            if !appDelegate.supportedRegion.containsCoordinate(location.coordinate) {
                narrationService.normalMessage("Attention, this area is not supported.")
            }
        }
        currentLocation = location
        checkGpsSignal()
        checkDangerousLocation()
    }
    
    func headingUpdated(heading: CLHeading) {
        currentHeading = heading
    }
}