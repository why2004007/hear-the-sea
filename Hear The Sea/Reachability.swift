//
//  Reachability.swift
//  FIT5120-Hear_The_Sea-Alpha
//
//  This part of code is retrieved from Internet
//  There's no license bound to this single part of code
//  URL: http://stackoverflow.com/questions/30743408/check-for-internet-connection-in-swift-2-ios-9
//

/*
    This piece of code is retrieved from stackoverflow
    Feature provided by this piece of code:
    Test whether the cellular network is enabled or not
    Source link:
    http://stackoverflow.com/questions/30743408/check-for-internet-connection-in-swift-2-ios-9
 */

import Foundation
import SystemConfiguration

public class Reachability {
    class func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(sizeofValue(zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(&zeroAddress) {
            SCNetworkReachabilityCreateWithAddress(nil, UnsafePointer($0))
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
}