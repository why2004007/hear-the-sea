//
//  RootViewController.swift
//  Hear The Sea
//
//  Created by Daniel Liu on 23/08/2016.
//  Copyright © 2016 Daniel Liu. All rights reserved.
//

import UIKit
import CoreLocation
import AVFoundation

class RootViewController: UIViewController, SpeechRecognitionServiceDelegate {

    @IBOutlet weak var weatherButton: UIButton!
    @IBOutlet weak var surroundingsButton: UIButton!
    @IBOutlet weak var recognitionButton: UIButton!
    @IBOutlet weak var navigationButton: UIButton!
    
    var stopButton: UIButton!
    
    var narrationService:NarrationService!
    var alarmService:AlarmService!
    var weatherInformationProvider:WeatherInformationProvider!
    var surroundingsInformationProvider:SurroundingsInformationProvider!
    var speechRecognitionService: SpeechRecognitionService!
    var nora: Nora!
    var navigationService: NavigationService!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Add long press detection to surrounding button
        let surroundingButtonLongPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(surroundingsButtonLongPressed))
        surroundingsButton.addGestureRecognizer(surroundingButtonLongPressRecognizer)
        
        
        // Add stop button programmatically
        stopButton = UIButton(type: .Custom)
        let screenSize: CGRect = UIScreen.mainScreen().bounds
        let buttonSize = screenSize.width * 0.4
        stopButton.frame = CGRectMake(160, 100, buttonSize, buttonSize)
        stopButton.center = self.view.center
        stopButton.layer.cornerRadius = 0.5 * stopButton.bounds.size.width
        stopButton.titleLabel?.font = UIFont.boldSystemFontOfSize(45)
        stopButton.setTitle("STOP", forState: .Normal)
        stopButton.backgroundColor = UIColor(red: 0.13, green: 0.52, blue: 1, alpha: 1)
        stopButton.addTarget(self, action: #selector(self.stopButtonClicked), forControlEvents: UIControlEvents.TouchUpInside)
        stopButton.setTitleColor(.grayColor(), forState: .Highlighted)
        view.addSubview(stopButton)
        
        // Setup all the services
        // We need the narration service to be initialised first so that we can use it to narrate welcome information
        narrationService = NarrationService()
        // Check whether we have gps service enabled and cellular network
        if performInitialCheck() {
            alarmService = AlarmService(aNarrationService: narrationService)
            weatherInformationProvider = WeatherInformationProvider(aNarrationService: narrationService)
            surroundingsInformationProvider = SurroundingsInformationProvider(aNarrationService: narrationService)
            navigationService = NavigationService(aNarrationService: narrationService, aSurroundingsInformationProvider: surroundingsInformationProvider)
            speechRecognitionService = SpeechRecognitionService(delegate: self)
            nora = Nora(aNarrationService: narrationService, aAlarmService: alarmService, aWeatherInformationProvider: weatherInformationProvider, aSurroundingsInformationProvider: surroundingsInformationProvider, aNavigationService: navigationService)
            narrationService.narrateUnstopableMessage("Welcome to Hear the Sea, Saint Kilda version. Please be aware of your surroundings while using our application. ")
            narrationService.normalMessage("You can know more about surroundings, weather and navigation using this application. In addition, you can long press surroundings button to get the detail about certain location. Also you can interact by talking to NORA by pressing the left bottom button. You may voice HELP to get the list of voice instructions. You will be notified of potential dangers surrounding you. This will not include moving objects, open manholes, broken pathways or sea water level. NORA understands only commands mentioned in the HELP guide.")
        } else {
            weatherButton.enabled = false
            surroundingsButton.enabled = false
            navigationButton.enabled = false
            recognitionButton.enabled = false
            stopButton.enabled = false
        }
        weatherButton.titleLabel!.layer.opacity = 0.0
        surroundingsButton.titleLabel!.layer.opacity = 0.0
        navigationButton.titleLabel!.layer.opacity = 0.0
        recognitionButton.titleLabel!.layer.opacity = 0.0
    }
    
    // Check location service and cellular network, if it is not working, narrate error message
    func performInitialCheck() -> Bool {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        var errorMessage = "Error! This app can only be used when "
        var flag = true
        if !appDelegate.locationServiceEnabled {
            errorMessage += "location service "
            flag = false
        }
        if !Reachability.isConnectedToNetwork() {
            errorMessage += "cellular network "
            flag = false
        }
        errorMessage += "is enabled. All the services will be disabled."
        if !flag {
            narrationService.normalMessage(errorMessage)
        }
        return flag
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // This function will check whether the system is ready to narrate
    func canNarrate(requestSender: CallbackSender!) -> Bool {
        return !narrationService.isReading() && (self.speechRecognitionService.state == SpeechRecognitionService.SKSState.Idle) && (!narrationService.isHandlingCallback() || ((narrationService.currentCallbackSender != nil) && (narrationService.currentCallbackSender == requestSender)))
    }
    
    // MARK: - Event Handling Functions
    
    @IBAction func recognitionButtonClicked(sender: AnyObject) {
        if !self.narrationService.isReading() {
            if self.speechRecognitionService.state != SpeechRecognitionService.SKSState.Idle {
                speechRecognitionService.reset()
            } else {
                self.speechRecognitionService.recognize()
            }
        }
    }
    
    @IBAction func weatherBtnClicked(sender: AnyObject) {
        if canNarrate(nil) {
            weatherInformationProvider.narrateWeather(WeatherInformationCategory.OverallWeather)
        }
    }

    @IBAction func surroundingsBtnClicked(sender: AnyObject) {
        if canNarrate(.Surrounding) {
            surroundingsInformationProvider.surroundingsButtonClicked()
        }
    }
    
    func speechKit(speechRecognitionService: SpeechRecognitionService, didFailedToInit info:String) {
        if canNarrate(nil) {
            surroundingsInformationProvider.narrateSurroundings()
        }
    }
    
    func speechKit(speechRecognitionService: SpeechRecognitionService, didReceiveRecognition text:String) {
        nora.commandReceived(text)
    }
    
    func speechKit(speechRecognitionService: SpeechRecognitionService, didFailedWithError error:NSError!) {
        if error.code != 4 {
            narrationService.normalMessage("I can not understand what you just said.")
        }
    }
    
    func stopButtonClicked() {
        surroundingsInformationProvider.reset()
        narrationService.reset()
        navigationService.reset()
        speechRecognitionService.reset()
    }
    
    
    @IBAction func navigationButtonClicked(sender: AnyObject) {
        if canNarrate(CallbackSender.Navigation) {
            navigationService.prepareToNavigate()
        }
    }
    
    func surroundingsButtonLongPressed(sender: UILongPressGestureRecognizer) {
        if sender.state == UIGestureRecognizerState.Ended {
            if canNarrate(CallbackSender.Detail) {
                surroundingsInformationProvider.detailButtonClicked()
            }
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
