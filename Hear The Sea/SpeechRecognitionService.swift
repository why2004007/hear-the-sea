//
//  SpeechRecognitionService.swift
//  FIT5120-Hear_The_Sea-Alpha
//
//  Created by Daniel Liu on 22/08/2016.
//  Copyright © 2016 Daniel Liu. All rights reserved.
//

import Foundation
import SpeechKit

protocol SpeechRecognitionServiceDelegate {
    func speechKit(speechRecognitionService: SpeechRecognitionService, didFailedToInit info:String)
    func speechKit(speechRecognitionService: SpeechRecognitionService, didReceiveRecognition text:String)
    func speechKit(speechRecognitionService: SpeechRecognitionService, didFailedWithError error:NSError!)
}

class SpeechRecognitionService : NSObject, SKTransactionDelegate {
    
    // State of the speech recognition service
    enum SKSState {
        case Idle
        case Listening
        case Processing
    }
    
    // Delegate used to receive the speech recognition states
    var delegate: SpeechRecognitionServiceDelegate?
    
    // The state of speech recognition service
    var state: SKSState!
    
    // The session created to transfer transaction
    var session: SKSession!
    
    // Current transaction
    var transaction: SKTransaction!
    
    // Initialize the class
    init(delegate: SpeechRecognitionServiceDelegate) {
        super.init()
        self.delegate = delegate
        self.state = .Idle
        session = SKSession(URL: NSURL(string: SKSServerUrl), appToken: SKSAppKey)
        if session == nil && self.delegate != nil {
            self.delegate?.speechKit(self, didFailedToInit: "Failed to create session.")
        }
        loadSoudEffects()
    }
    
    // This method will load all the sound resources to notify the user that the state of speech recognition service
    func loadSoudEffects() {
        let startEarconPath = NSBundle.mainBundle().pathForResource("sk_start", ofType: "pcm")
        let stopEarconPath = NSBundle.mainBundle().pathForResource("sk_stop", ofType: "pcm")
        let errorEarconPath = NSBundle.mainBundle().pathForResource("sk_error", ofType: "pcm")
        let audioFormat = SKPCMFormat()
        audioFormat.sampleFormat = .SignedLinear16
        audioFormat.sampleRate = 16000
        audioFormat.channels = 1
        session!.startEarcon = SKAudioFile(URL: NSURL(fileURLWithPath: startEarconPath!), pcmFormat: audioFormat)
        session!.endEarcon = SKAudioFile(URL: NSURL(fileURLWithPath: stopEarconPath!), pcmFormat: audioFormat)
        session!.errorEarcon = SKAudioFile(URL: NSURL(fileURLWithPath: errorEarconPath!), pcmFormat: audioFormat)
    }
    
    // Start to recognize the voices
    func recognize() {
        self.state = .Listening
        self.transaction = self.session!.recognizeWithType(SKTransactionSpeechTypeDictation, detection: .Short, language: SKSLanguage, delegate: self)
    }
    
    // Stop all the transactions
    func reset() {
        if self.transaction != nil {
            self.transaction!.cancel()
        }
    }
    
    // MARK: - Delegate Functions: SKTransactionDelegate
    
    // Called when the trasaction begin record voices
    func transactionDidBeginRecording(transaction: SKTransaction!) {
        state = .Listening
    }
    
    // Called when the recording is finished
    func transactionDidFinishRecording(transaction: SKTransaction!) {
        state = .Processing
    }
    
    // Called when recognition is received
    func transaction(transaction: SKTransaction!, didReceiveRecognition recognition: SKRecognition!) {
        if self.delegate != nil {
            self.delegate!.speechKit(self, didReceiveRecognition: recognition.text)
        }
        state = .Idle
    }
    
    // Called when suggestion is received
    func transaction(transaction: SKTransaction!, didFinishWithSuggestion suggestion: String) {
        state = .Idle
        self.transaction = nil
    }
    
    // Called when error is occured
    func transaction(transaction: SKTransaction!, didFailWithError error: NSError!, suggestion: String) {
        if self.delegate != nil {
            self.delegate!.speechKit(self, didFailedWithError: error)
        }
        state = .Idle
        self.transaction = nil
    }
    
}