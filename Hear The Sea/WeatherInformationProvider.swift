//
//  WeatherInformationProvider.swift
//  FIT5120-Hear_The_Sea-Alpha
//
//  Created by Daniel Liu on 22/08/2016.
//  Copyright © 2016 Daniel Liu. All rights reserved.
//

import Foundation
import Gloss
import CoreLocation

// This enumerate is used to classify the weather info category
enum WeatherInformationCategory {
    case OverallWeather, WeatherConditions, CurrentTemperature, UVIndex, WindSpeed
}

class WeatherInformationProvider : LocationInformationDelegate {
    
    
    // OpenWeatherMap.org API
    let weatherAPIUrl = "http://api.openweathermap.org/data/2.5/forecast?"
    let weatherAPIParameters = "units=metric"
    let weatherAPIKey = "248efdeb4891301e4f6eaa3fd72deb6f"
    let uvIndexAPIUrl = "http://api.openweathermap.org/v3/uvi/"
    let uvIndexAPIParameters = "/current.json?"
    
    // Store the weather api responses
    var weatherInfo:WeatherAPIResponse!
    var uvIndexInfo:UVIndexResponse!
    
    // Store current location
    var currentLocation: CLLocation!
    
    // Store the location which weather is updated last time
    var lastUpdateLocation: CLLocation!
    
    // URLSession
    let urlSession: NSURLSession
    
    // Narration Service
    let narrationService: NarrationService
    
    // Weather Updating Timer
    var weatherUpdatingTimer: NSTimer!
    
    // Initialize the calss
    init(aNarrationService: NarrationService) {
        narrationService = aNarrationService
        
        // Initialize urlsession
        let urlSessionConfig = NSURLSessionConfiguration.defaultSessionConfiguration()
        urlSessionConfig.allowsCellularAccess = true
        urlSession = NSURLSession(configuration: urlSessionConfig, delegate: nil, delegateQueue: nil)
        
        // Register location update notification service
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.registerLocationInformationDelegate(self)
        
        // Set up weather timer
        weatherUpdatingTimer = NSTimer(timeInterval: 300, target: self, selector: #selector(updateInformation), userInfo: nil, repeats: true)
        NSRunLoop.mainRunLoop().addTimer(weatherUpdatingTimer, forMode: NSRunLoopCommonModes)
    }
    
    // This function will call both updates functions, used by timer
    @objc func updateInformation() {
        lastUpdateLocation = currentLocation
        updateWeather()
        updateUVIndex()
    }
    
    // This function updates the weather
    func updateWeather() {
        let weatherInfoTask = urlSession.dataTaskWithRequest(NSURLRequest(URL: getWeatherAPIUrlBasedOnCurrentLocation()), completionHandler: { (data: NSData?, response: NSURLResponse?, error: NSError?) in
            do {
                //print(String(data: data!, encoding: NSUTF8StringEncoding)!)
                let weatherInfoJson = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! JSON
                self.weatherInfo = WeatherAPIResponse(json: weatherInfoJson)
            }
            catch {
                self.weatherInfo = nil
            }
        })
        weatherInfoTask.resume()
    }
    
    // This function updates the uvIndex
    func updateUVIndex() {
        let uvIndexTask = urlSession.dataTaskWithRequest(NSURLRequest(URL: getUVIndexAPIUrlBasedOnCurrentLocation()), completionHandler: { (data: NSData?, response: NSURLResponse?, error: NSError?) in
            do {
                let uvIndexJson = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! JSON
                self.uvIndexInfo = UVIndexResponse(json: uvIndexJson)
            }
            catch {
                self.uvIndexInfo = nil
            }
        })
        uvIndexTask.resume()
    }
    
    // This function controls the narration of weather
    func narrateWeather(type: WeatherInformationCategory) {
        if weatherInfo != nil && uvIndexInfo != nil {
            var result = ""
            switch type {
            case .OverallWeather: result = getWeather()
            case .WeatherConditions: result = getWeatherCondition()
            case .CurrentTemperature: result = getCurrentTemperature()
            case .UVIndex: result = getUvIndex()
            case .WindSpeed: result = getWindSpeed()
            }
            narrationService.normalMessage(result)
        } else {
            narrationService.normalMessage("Weather information has not been loaded. Please try again later.")
        }
    }
    
    // This function will return overall weather information
    func getWeather() -> String {
        return "\(getCurrentTemperature()) \(getWeatherCondition()) \(getWindSpeed()) \(getUvIndex())"
    }
    
    // This function will return weather conditions
    func getWeatherCondition() -> String {
        var sentence = "Current weather is: \(self.weatherInfo.list.first!.weather.first!.descriptionField). "
        var index = 0
        for item in weatherInfo!.list {
            if item.rain != nil {
                if item.rain.volume != nil {
                    if index != 0 {
                        sentence += "It will rain after \(index * 3) hours."
                        break
                    }
                }
            }
            if item.snow != nil {
                if item.snow.volume != nil {
                    if index != 0 {
                        sentence += "It will snow after \(index * 3) hours."
                        break
                    }
                }
            }
            index += 1
        }
        return sentence
    }
    
    // This function will return current temperature
    func getCurrentTemperature() -> String {
        return "Current temperature is: \(Int(self.weatherInfo.list[0].main.tempMin)) degree celsius."
    }
    
    
    // This function will return wind speed
    func getWindSpeed() -> String {
        return "Current wind speed is \(Int(weatherInfo!.list.first!.wind.speed)) meter per hour."
    }
    
    // This function will retrun uv index info
    func getUvIndex() -> String {
        let uvIndex = self.uvIndexInfo!.data!
        if uvIndex < 2.9 {
            return "The uv index is low, wear a pair of sun glasses is enough."
        }
        if uvIndex < 5.9 {
            return "The uv index is moderate, please take precautions, such as covering up, if you will be outside. "
        }
        if uvIndex < 7.9 {
            return "The uv index is high. You should cover the body with sun protective clothing, use SPF 30+ sunscreen, wear a hat, reduce time in the sun within three hours of solar noon, and wear sunglasses."
        }
        if uvIndex < 10.9 {
            return "The uv index is very high. You should wear SPF 30+ sunscreen, a shirt, sunglasses, and a wide-brimmed hat. Do not stay in the sun for too long."
        }
        if uvIndex > 11 {
            return "The uv index is very high. Take all precautions, such as wear SPF 30+ sunscreen, a long-sleeved shirt and trousers, sunglasses, and a very broad hat. Avoid the sun within three hours of solar noon."
        }
        return "Error when processing uv index level. Please try again later."
    }
    
    // Check if user have a great movement of their location
    func checkHugeMovement() {
        if lastUpdateLocation != nil {
            if lastUpdateLocation.distanceFromLocation(currentLocation) > 1000 {
                updateInformation()
            }
        }
    }
    
    // Functions to return weather api url
    func getWeatherAPIUrlBasedOnCurrentLocation() -> NSURL {
        return NSURL(string: "\(weatherAPIUrl)lat=\(currentLocation!.coordinate.latitude)&lon=\(currentLocation!.coordinate.longitude)&\(weatherAPIParameters)&APPID=\(weatherAPIKey)")!
    }
    
    func getUVIndexAPIUrlBasedOnCurrentLocation() -> NSURL {
        return NSURL(string: "\(uvIndexAPIUrl)\(Int(currentLocation!.coordinate.latitude)),\(Int(currentLocation!.coordinate.longitude))\(uvIndexAPIParameters)&APPID=\(weatherAPIKey)")!
    }
    
    // MARK: - Delegate Functions: LocationInformationDelegate
    func locationUpdated(location: CLLocation) {
        if currentLocation == nil {
            currentLocation = location
            updateInformation()
        } else {
            currentLocation = location
        }
    }
    
}
