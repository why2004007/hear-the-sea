//
//  Utilities.swift
//  Hear The Sea
//
//  Created by Daniel Liu on 20/09/2016.
//  Copyright © 2016 Daniel Liu. All rights reserved.
//

import Foundation
import CoreLocation

class Utilities {
    
    // Calculate distance by coordinates
    static func calculateDistanceBetweenCoordinates(startPoint: CLLocationCoordinate2D, endPoint: CLLocationCoordinate2D) -> CLLocationDistance {
        let startLocation = CLLocation(coordinate: startPoint, altitude: 0, horizontalAccuracy: 0, verticalAccuracy: 0, course: 0, speed: 0, timestamp: NSDate())
        let endLocation = CLLocation(coordinate: endPoint, altitude: 0, horizontalAccuracy: 0, verticalAccuracy: 0, course: 0, speed: 0, timestamp: NSDate())
        return startLocation.distanceFromLocation(endLocation)
    }
    
    // Convert latitude and longitude to coordinate
    static func getCoordinate(lat: Double, lon: Double) -> CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: lat, longitude: lon)
    }
    
    static func degreeToRadians(degree: Double) -> Double {return degree * M_PI / 180}
    static func radiansToDegree(radians: Double) -> Double {return radians * 180 / M_PI}
    
    // This function can calculate the geo angle between two locations
    static func getGeoAngleBetweenTwoPoints(center: CLLocation, targetLocation: CLLocation) -> Double {
        
        let centerLatitude = degreeToRadians(center.coordinate.latitude)
        let centerLongitude = degreeToRadians(center.coordinate.longitude)
        
        let targetLatitude = degreeToRadians(targetLocation.coordinate.latitude)
        let targetLongitude = degreeToRadians(targetLocation.coordinate.longitude)
        
        let deltaLongitude = targetLongitude - centerLongitude
        
        let X = cos(targetLatitude) * sin(deltaLongitude)
        let Y = cos(centerLatitude) * sin(targetLatitude) - sin(centerLatitude) * cos(targetLatitude) * cos(deltaLongitude)
        
        var radiansBearing = atan2(X, Y)
        if radiansBearing < 0 {
            radiansBearing += 2 * M_PI
        }
        
        return radiansToDegree(radiansBearing)
    }
}

