//
//  NavigationService.swift
//  FIT5120-Hear_The_Sea-Alpha
//
//  Created by Daniel Liu on 22/08/2016.
//  Copyright © 2016 Daniel Liu. All rights reserved.
//

import Foundation
import CoreLocation
import Gloss
import AVFoundation

// This enumerate will be used to classify which type of destination selection is being used.
enum DestinationSelectionPhase {
    case NearestLocations, Category
}

class NavigationService : LocationInformationDelegate {
    
    // Narration service used to convert text to speech
    var narrationService: NarrationService
    
    // Surrounding information variables
    var surroundingsInformationProvider: SurroundingsInformationProvider
    var placesToGo: [Place]!
    
    // Variables used to handle location selection process
    var currentProcessingIndex: Int = 0
    var userConfirmed: Bool = false
    var isSelectingDestination: Bool = false
    var preparingToRedirectToCategoryNarration: Bool = false
    var destinationSelectingPhase: DestinationSelectionPhase = .NearestLocations
    var categoryList = [PlaceCategory.PublicInfrustructure, PlaceCategory.Landmarks, PlaceCategory.Food, PlaceCategory.Transport, PlaceCategory.Entertainment, PlaceCategory.Medical]
    var userConfirmedCategory: PlaceCategory!
    var correspondingTypeList: [PlaceType]!
    var userConfirmedType: PlaceType!
    var correspondingPlaceList: [Place]!
    
    // Store current location
    var currentLocation: CLLocation!
    
    // Store settings about help of navigation
    var needNavigationHelp: Bool = true
    
    // Initializer
    init(aNarrationService: NarrationService, aSurroundingsInformationProvider: SurroundingsInformationProvider) {
        narrationService = aNarrationService
        surroundingsInformationProvider = aSurroundingsInformationProvider
        // Load stored settings
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        needNavigationHelp = appDelegate.settings!["NavigationHelpEnabled"] as! Bool
        // Register location service
        appDelegate.registerLocationInformationDelegate(self)
    }
    
    // Prepare the information for navigation
    func prepareToNavigate() {
        placesToGo = surroundingsInformationProvider.getSurroundingsForNavigation()
        if placesToGo != nil {
            if !isSelectingDestination {
                isSelectingDestination = true
                if needNavigationHelp {
                    narrationService.narrateWithCallback(CallbackSender.Navigation, string: "I'm going to tell you a list of the locations. Between each location, there will be a pause, if you want to be navigated to there, please press navigation button again. This message can be disabled in the settings. Or you can use speech recognition feature and say: disable help of navigation to disable this help.", callbackInterval: 0.1, callback: {
                        self.selectingDestination()
                    })
                } else {
                    selectingDestination()
                }
                
            } else {
                userConfirmed = true
                narrationService.resetCallback()
                selectingDestination()
            }
            
        } else {
            narrationService.normalMessage("The location information hasn't been loaded, please try again later.")
        }
    }
    
    // Start to select destination
    func selectingDestination() {
        if destinationSelectingPhase == .NearestLocations {
            // Handle the nearest location selection process
            // First test whether has clicked button to confirm
            if userConfirmed {
                userConfirmed = false
                // User confirmed location within the nearest locations
                if currentProcessingIndex < 5 {
                    destinationSelected(placesToGo![currentProcessingIndex - 1].location!)
                } else {
                    // We will reuse this variable to process category list
                    currentProcessingIndex = 0
                    // Disable this flag because we've already switched to category selection
                    preparingToRedirectToCategoryNarration = false
                    destinationSelectingPhase = .Category
                    selectingDestination()
                    return
                }
            } else {
                if currentProcessingIndex == 0 {
                    narrationService.normalMessage("Do you want to go to ")
                }
                // If user hasn't confirmed the selection, process the following options
                // First ask user to select destination from 5 nearest locations
                if currentProcessingIndex < 5 {
                    narrationService.narrateWithCallback(CallbackSender.Navigation, string: "\(placesToGo[currentProcessingIndex].name)", callbackInterval: 2, callback: {
                        self.selectingDestination()
                    })
                    currentProcessingIndex += 1
                    return
                }
                // If they do not find the useful information, ask them to select by category
                if !preparingToRedirectToCategoryNarration{
                    preparingToRedirectToCategoryNarration = true
                    narrationService.narrateWithCallback(CallbackSender.Navigation, string: "If you want to select destination by category, please press navigation button again.", callbackInterval: 2, callback: {
                        self.selectingDestination()
                    })
                    return
                }
                // If they do not want to select by category, reset navigation
                if preparingToRedirectToCategoryNarration {
                    narrationService.normalMessage("Failed to select destination, please try again.")
                    reset()
                }
            }
        } else {
            if userConfirmed {
                userConfirmed = false
                // Handle user confirming category
                if userConfirmedCategory == nil {
                    // Set category to user selected category
                    userConfirmedCategory = categoryList[currentProcessingIndex - 1]
                    // Load type list
                    correspondingTypeList = SurroundingsInformationProvider.placeCategoryToType(userConfirmedCategory)
                    // Reset index
                    currentProcessingIndex = 0
                    selectingDestination()
                    return
                }
                // Handle user confirming type
                if userConfirmedType == nil {
                    // Set user confirmed type
                    userConfirmedType = correspondingTypeList![currentProcessingIndex - 1]
                    // Fill up the target list with places which is corresponding type
                    correspondingPlaceList = [Place]()
                    for place in placesToGo! {
                        if place.type == userConfirmedType {
                            correspondingPlaceList.append(place)
                        }
                    }
                    // Reset index
                    currentProcessingIndex = 0
                    selectingDestination()
                    return
                }
                // Handle user confirming places
                destinationSelected(correspondingPlaceList![currentProcessingIndex - 1].location!)
            } else {
                // Narrate category for user to select
                if userConfirmedCategory == nil {
                    if currentProcessingIndex == 0 {
                        narrationService.normalMessage("Do you want to go to ")
                    }
                    if currentProcessingIndex < categoryList.count {
                        narrationService.narrateWithCallback(CallbackSender.Navigation, string: "\(categoryList[currentProcessingIndex].rawValue)", callbackInterval: 2, callback: {
                            self.selectingDestination()
                        })
                        currentProcessingIndex += 1
                    } else {
                        narrationService.normalMessage("Failed to select category, please try again later.")
                        reset()
                    }
                    return
                }
                // Narrate place type for user to select
                if userConfirmedType == nil {
                    if currentProcessingIndex < correspondingTypeList!.count {
                        let typeName = correspondingTypeList![currentProcessingIndex].rawValue
                        let readableTypeName = typeName.stringByReplacingOccurrencesOfString("_", withString: " ")
                        narrationService.narrateWithCallback(CallbackSender.Navigation, string: "\(readableTypeName)", callbackInterval: 2, callback: {
                            self.selectingDestination()
                        })
                        currentProcessingIndex += 1
                    } else {
                        narrationService.normalMessage("Failed to select place type, please try again later.")
                        reset()
                    }
                    return
                }
                // Narrate place for user to select destination
                if correspondingPlaceList.count != 0 {
                    if userConfirmedCategory == PlaceCategory.PublicInfrustructure {
                        destinationSelected(correspondingPlaceList!.first!.location!)
                        return
                    }
                    if currentProcessingIndex < correspondingPlaceList!.count {
                        narrationService.narrateWithCallback(CallbackSender.Navigation, string: "\(correspondingPlaceList![currentProcessingIndex].name)", callbackInterval: 2, callback: {
                            self.selectingDestination()
                        })
                        currentProcessingIndex += 1
                    } else {
                        narrationService.normalMessage("Failed to select destination, please try again.")
                        reset()
                    }
                } else {
                    narrationService.normalMessage("There are no locations in this category. Please try again.")
                    reset()
                }
            }
        }
    }
    
    // Intermmediate function which tell the user the up coming change
    func destinationSelected(destination: CLLocation) {
        reset()
        narrationService.narrateWithCallback(CallbackSender.Navigation, string: "You are going to be redirected to apple map, once the transition finished, you can click the center of bottom part of the screen to start navigation.", callbackInterval: 0.5) {
            self.startNavigation(destination)
        }
        
    }
    
    // Redirect to apple map to provide navigation
    func startNavigation(destination: CLLocation) {
        let app = UIApplication.sharedApplication()
        let navigationUrl = NSURL(string: "http://maps.apple.com/maps?saddr=\(currentLocation.coordinate.latitude),\(currentLocation.coordinate.longitude)&daddr=\(destination.coordinate.latitude),\(destination.coordinate.longitude)&dirflg=w")
        app.openURL(navigationUrl!)
    }
    
    // This function will be used to reset navigation
    func reset() {
        currentProcessingIndex = 0
        userConfirmed = false
        isSelectingDestination = false
        preparingToRedirectToCategoryNarration = false
        destinationSelectingPhase = .NearestLocations
        userConfirmedCategory = nil
        correspondingTypeList = nil
        userConfirmedType = nil
    }
    
    // MARK: - Delegate Functions: LocationInformationDelegate
    func locationUpdated(location: CLLocation) {
        currentLocation = location
    }
}
