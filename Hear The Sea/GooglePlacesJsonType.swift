//
//  PlacesAPIResponse.swift
//  FIT5120-Hear_The_Sea-Alpha
//
//  Created by Daniel Liu on 22/08/2016.
//  Copyright © 2016 Daniel Liu. All rights reserved.
//

import Foundation
import Gloss

struct Location : Decodable {
    
    let lat : Double!
    let lng : Double!
    
    init?(json: JSON){
        lat = "lat" <~~ json
        lng = "lng" <~~ json
    }
}

struct Geometry : Decodable {
    
    let location : Location!
    
    init?(json: JSON){
        location = "location" <~~ json
    }

}

struct GooglePlacesSearchResult : Decodable {
    
    let geometry : Geometry!
    let icon : String!
    let id : String!
    let name : String!
    let placeId : String!
    let reference : String!
    let scope : String!
    let types : [String]!
    let vicinity : String!
    
    init?(json: JSON){
        geometry = "geometry" <~~ json
        icon = "icon" <~~ json
        id = "id" <~~ json
        name = "name" <~~ json
        placeId = "place_id" <~~ json
        reference = "reference" <~~ json
        scope = "scope" <~~ json
        types = "types" <~~ json
        vicinity = "vicinity" <~~ json
    }
}

struct GooglePlacesAPIResponse : Decodable {

    let results : [GooglePlacesSearchResult]!
    let status : String!
    let nextPageToken : String!
    
    init?(json: JSON){
        results = "results" <~~ json
        status = "status" <~~ json
        nextPageToken = "next_page_token" <~~ json
    }
}

