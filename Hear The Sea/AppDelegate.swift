//
//  AppDelegate.swift
//  Hear The Sea
//
//  Created by Daniel Liu on 23/08/2016.
//  Copyright © 2016 Daniel Liu. All rights reserved.
//

import UIKit
import CoreLocation
import AVFoundation

protocol LocationInformationDelegate {
    func locationUpdated(location: CLLocation)
}

protocol HeadingInformationDelegate {
    func headingUpdated(heading: CLHeading)
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate {

    var window: UIWindow?
    // Variables for location manager
    var locationManager = CLLocationManager()
    var locationServiceEnabled = false
    var locationInformationDelegates = [LocationInformationDelegate]()
    var headingInformationDelegates = [HeadingInformationDelegate]()
    
    // To improve accuracy of location, we need to filter some noise
    var lastLocation: CLLocation!
    
    // Supported region detection variables
    var supportedRegion: CLCircularRegion!
    
    // Settings
    var settings: NSMutableDictionary!
    var settingsPath: String!
    
    var audioPlayer: AVAudioPlayer!
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
        enableBackgroundNarration()
        initializeLocationManager()
        loadSettings()
        // Load the supported area data from settings
        let location = settings!["StKildaArea"] as! NSDictionary
        let latitude = location["latitude"] as! Double
        let longitude = location["longitude"] as! Double
        let center = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        // Set the supported area
        supportedRegion = CLCircularRegion(center: center, radius: 1000, identifier: "Supported Region")
        
        let filePath = NSBundle.mainBundle().pathForResource("silentMusic", ofType: "wav")
        let url = NSURL(fileURLWithPath: filePath!)
        do {
            audioPlayer = try AVAudioPlayer(contentsOfURL: url)
            audioPlayer.play()
        } catch{}
        
        return true
    }
    
    // Load settings from file
    func loadSettings() {
        // Check if settings exists within users domain
        let directoryPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)[0] as NSString
        settingsPath = directoryPath.stringByAppendingPathComponent("Settings.plist")
        let fileManager = NSFileManager()
        if !fileManager.fileExistsAtPath(settingsPath) {
            let originalSettingsPath = NSBundle.mainBundle().pathForResource("Settings", ofType: "plist")!
            do {
                try fileManager.copyItemAtPath(originalSettingsPath, toPath: settingsPath)
            } catch {}
        }
        settings = NSMutableDictionary(contentsOfFile: settingsPath)
    }
    
    // Save settings to file
    func saveSettings() {
        if settingsPath != nil {
            settings.writeToFile(settingsPath!, atomically: true)
        }
    }
    
    // Set Nora to work when it is in the background and the silent mode is enabled
    func enableBackgroundNarration() {
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)
        } catch {
            print("Something wrong happened. I can not work at background.")
        }
    }
    
    // Initialize the location manager
    func initializeLocationManager() {
        locationManager.requestAlwaysAuthorization()
        locationServiceEnabled = CLLocationManager.locationServicesEnabled()
        if locationServiceEnabled {
            // Set current appdelegate as location manager's delegate
            locationManager.delegate = self
            // Set accuracy to the best accuracy
            locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
            // Start update location and heading informationa when the app is started.
            locationManager.startUpdatingLocation()
            locationManager.startUpdatingHeading()
        }
    }
    
    // Allow other services to register as a delegate to receive messages
    func registerLocationInformationDelegate(delegate: LocationInformationDelegate) {
        locationInformationDelegates.append(delegate)
    }
    
    func registerHeadingInformationDelegate(delegate: HeadingInformationDelegate) {
        headingInformationDelegates.append(delegate)
    }
    
    // MARK: - Delegate Functions: Location Manager Delegate
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if lastLocation == nil {
            lastLocation = locations.first!
        } else {
            // Discard the movement larger than half of horizontal accuracy
            if lastLocation.timestamp.timeIntervalSinceNow < -3 || lastLocation.distanceFromLocation(locations.first!) < locations.first!.horizontalAccuracy * 0.5 {
                lastLocation = locations.first!
                for delegate in locationInformationDelegates {
                    delegate.locationUpdated(locations.first!)
                }
            }
        }
        
    }
    
    func locationManager(manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        for delegate in headingInformationDelegates {
            delegate.headingUpdated(newHeading)
        }
    }
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        UIApplication.sharedApplication().beginBackgroundTaskWithExpirationHandler {
            self.locationManager = CLLocationManager()
        }
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

