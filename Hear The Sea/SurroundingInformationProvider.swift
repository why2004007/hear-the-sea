//
//  SurroundingInformationProvider.swift
//  FIT5120-Hear_The_Sea-Alpha
//
//  Created by Daniel Liu on 22/08/2016.
//  Copyright © 2016 Daniel Liu. All rights reserved.
//

import Foundation
import CoreLocation
import Gloss

class SurroundingsInformationProvider : LocationInformationDelegate, HeadingInformationDelegate {
    
    // Create sessions for surrounding information provider to retrieve information
    let urlSession: NSURLSession
    
    // Google Places API URL & Parameters used to refine search
    let googlePlacesAPIUrl = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?"
    let APIParametersSearchLanguage = "language=en"
    let APIParametersRadius = "radius=500"
    let googlePlacesNextPageUrl = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?"
    let googlePlacesAPIKey = "AIzaSyBR8Ygb9wpRKFboDNjUJr4ZCKIvHR__S-M"
    
    // Variables used to retrieve all the locations from google
    var nextPageAvailable = false
    var nextPageToken: String!
    
    // This variable is used to store all the places returned by search
    var surroundings = [GooglePlacesSearchResult]()
    
    // This variable stores the places sorted by the distance from user's current location
    var localStoredPlaces: [Place]!
    var sortedPlacesForNavigation = [Place]()
    var sortedPlacesForSurroundingsAndDetail = [Place]()
    var placesHaveBeenSorted: Bool = false
    
    // The narration service used to perform the text to speech feature
    var narrationService: NarrationService
    
    // The variables used to determine the location and heading
    var currentLocation: CLLocation!
    var currentHeading: CLHeading!
    
    // Handle the narrate surroundings feature
    var currentReadingIndex = 0
    var isReadingFiveSurroundings = false
    
    // Handle the read more feature
    var handleMoreSurroundings: Bool = false
    var alreadyReadIndex: Int = 0
    
    // Handle the detail feature
    var handleDetailOfSurroudings: Bool = false
    var detailButtonClickedAgain: Bool = false
    var lastReadIndex: Int = 0
    
    // Setup Automatic Update Surrounding Information
    var updateSurroundingsTimer: NSTimer!
    var lastUpdateLocation: CLLocation!
    
    // Initialize the object
    init(aNarrationService: NarrationService) {
        narrationService = aNarrationService
        
        // Initialize urlsession
        let urlSessionConfig = NSURLSessionConfiguration.defaultSessionConfiguration()
        urlSessionConfig.allowsCellularAccess = true
        urlSession = NSURLSession(configuration: urlSessionConfig, delegate: nil, delegateQueue: nil)
        
        // Register myself to AppDelegate to receive location information and heading information
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.registerLocationInformationDelegate(self)
        appDelegate.registerHeadingInformationDelegate(self)
        
        // Setup automatic update surroundings timer
        updateSurroundingsTimer = NSTimer(timeInterval: 60, target: self, selector: #selector(checkLocationUpdates), userInfo: nil, repeats: true)
        NSRunLoop.mainRunLoop().addTimer(updateSurroundingsTimer, forMode: NSRunLoopCommonModes)
        
        // Load local interesting location from settings
        localStoredPlaces = [Place]()
        let locationList = appDelegate.settings!["StKildaData"] as! [NSDictionary]
        for location in locationList {
            let typeString = location["type"] as! String
            let type = PlaceType(rawValue: typeString)!
            let category = SurroundingsInformationProvider.placeTypeToCategory(type)
            if category != .DangerousLocation {
                let locationDict = location["location"] as! NSDictionary
                let latitude = locationDict["latitude"] as! Double
                let longitude = locationDict["longitude"] as! Double
                let realLocation = CLLocation(latitude: latitude, longitude: longitude)
                let radius = location["radius"] as! Int
                var name = type.rawValue
                var detail = "It is a \(type.rawValue)."
                if type == .sculpture {
                    name = location["SculptureName"] as! String
                    detail = location["Detail"] as! String
                }
                let place = Place(aName: name, aCategory: category, aType: type, aRadius: radius, aLocation: realLocation, aRoute: nil, aDetail: detail)
                localStoredPlaces.append(place)
            }
        }
    }
    
    // Deal with the button events
    func surroundingsButtonClicked() {
        // Judge whether places have been sorted
        if placesHaveBeenSorted {
            sortSurroundingsByDistanceToCurrentLocation()
            if handleMoreSurroundings {
                narrationService.resetCallback()
            }
            narrateSurroundings()
            
        } else {
            // If the list hasn't been sorted, read the warnings.
            narrationService.normalMessage("Surrounding information hasn't been loaded, please try again later.")
        }
    }
    
    // Read five surrounding locations
    func narrateSurroundings() {
        
        if !handleMoreSurroundings {
            // Read out the instruction
            narrationService.normalMessage("You are surrounded by: ")
        }
        
        if currentReadingIndex == 0 || !isReadingFiveSurroundings {
            isReadingFiveSurroundings = true
            narrateFiveNearestSurroundings()
        }
    }
    
    // Narrate Five Nearest Surroundings
    func narrateFiveNearestSurroundings() {
        
        let targetLocation = sortedPlacesForSurroundingsAndDetail[currentReadingIndex].location!
        // First read out the name of narration service
        narrationService.normalMessage("\(sortedPlacesForSurroundingsAndDetail[currentReadingIndex].name).")
        // Then read out the distance from current location
        narrationService.normalMessage("\(Int(targetLocation.distanceFromLocation(currentLocation))) meters away.")
        // Last, read out the direction
        var directionText = ""
        let direction = currentHeading.trueHeading
        let targetDirection = Utilities.getGeoAngleBetweenTwoPoints(currentLocation, targetLocation: sortedPlacesForSurroundingsAndDetail[currentReadingIndex].location!)
        let directionBasedOnCurrentHeading = targetDirection - direction < 0 ? 360 + targetDirection - direction : targetDirection - direction
        if directionBasedOnCurrentHeading < 45 || directionBasedOnCurrentHeading > 315 {
            directionText = "It is in front of you. "
        }
        if directionBasedOnCurrentHeading > 45 && directionBasedOnCurrentHeading < 135 {
            directionText = "It is on you right hand side. "
        }
        if directionBasedOnCurrentHeading > 135 && directionBasedOnCurrentHeading < 225 {
            directionText = "It is located at the back of you. "
        }
        if directionBasedOnCurrentHeading > 225 && directionBasedOnCurrentHeading < 315 {
            directionText = "It is on your left hand side. "
        }
        narrationService.narrateWithCallback(CallbackSender.Surrounding, string: "\(directionText)", callbackInterval: 0.5, callback: { 
            self.narrateFiveNearestSurroundings()
        })
        currentReadingIndex += 1
        
        if currentReadingIndex % 5 == 0 || currentReadingIndex + 1 == sortedPlacesForSurroundingsAndDetail.count {
            isReadingFiveSurroundings = false
            handleMoreSurroundingsOption()
            return
        }
    }
    
    // Handle more surroundings feature
    func handleMoreSurroundingsOption() {
        if currentReadingIndex + 1 != sortedPlacesForSurroundingsAndDetail.count {
            handleMoreSurroundings = true
            narrationService.narrateWithCallback(CallbackSender.Surrounding, string: "Do you want to know more surroundings? Please press the surroundings button again within three seconds.", callbackInterval: 3, callback: {
                self.resetMoreSurroundings()
                self.resetNearestSurroundings()
            })
        } else {
            narrationService.normalMessage("No more surroundings.")
            self.resetNearestSurroundings()
        }
    }
    
    // Reset narration of five nearest surroundings
    func resetNearestSurroundings() {
        isReadingFiveSurroundings = false
        currentReadingIndex = 0
    }
    
    // Deal with detail button events
    func detailButtonClicked() {
        if placesHaveBeenSorted {
            sortSurroundingsByDistanceToCurrentLocation()
            if handleDetailOfSurroudings {
                narrationService.resetCallback()
                detailButtonClickedAgain = true
            }
            narrateDetails()
        } else {
            narrationService.normalMessage("Detail information hasn't been loaded. Please try again later.")
        }
    }
    
    // This function will narrate the details about surroundings
    func narrateDetails() {
        if handleDetailOfSurroudings {
            if detailButtonClickedAgain {
                detailButtonClickedAgain = false
                narrationService.normalMessage("\(sortedPlacesForSurroundingsAndDetail[lastReadIndex].detail)")
                resetDetailProcess()
            } else {
                lastReadIndex += 1
                if lastReadIndex < sortedPlacesForSurroundingsAndDetail.count - 1 {
                    narrationService.narrateWithCallback(CallbackSender.Detail, string: "\(sortedPlacesForSurroundingsAndDetail[lastReadIndex].name)", callbackInterval: 3, callback: {
                        self.narrateDetails()
                    })
                } else {
                    resetDetailProcess()
                    narrationService.normalMessage("Failed to select place to narrate details. Please try again.")
                }
            }
            
        } else {
            handleDetailOfSurroudings = true
            // Read out the instruction
            narrationService.normalMessage("Do you want to know details about: ")
            narrationService.narrateWithCallback(CallbackSender.Detail, string: "\(sortedPlacesForSurroundingsAndDetail[lastReadIndex].name)", callbackInterval: 3, callback: {
                self.narrateDetails()
            })
        }
    }
    
    // Reset the process of narrate details
    func resetDetailProcess() {
        handleDetailOfSurroudings = false
        lastReadIndex = 0
    }
    
    // This function is used to provide surroundings list to navigation feature
    func getSurroundingsForNavigation() -> [Place]! {
        if placesHaveBeenSorted {
            return sortedPlacesForNavigation
        } else {
            return nil
        }
    }
    
    // Reset the reading progress
    func resetMoreSurroundings() {
        handleMoreSurroundings = false
        alreadyReadIndex = 0
        currentReadingIndex = 0
    }
    
    // Total reset for stop button
    func reset() {
        resetDetailProcess()
        resetMoreSurroundings()
    }
    // Functions used to generate urls
    
    func getGooglePlacesSearchURLBasedOnCurrentLocation() -> NSURL {
        return NSURL(string: "\(googlePlacesAPIUrl)location=\(currentLocation.coordinate.latitude),\(currentLocation.coordinate.longitude)&\(APIParametersSearchLanguage)&\(APIParametersRadius)&key=\(googlePlacesAPIKey)")!
    }
    
    func getGooglePlacesNextPageURL(pageToken: String) -> NSURL {
        return NSURL(string: "\(googlePlacesNextPageUrl)pagetoken=\(pageToken)&key=\(googlePlacesAPIKey)")!
    }
    
    // This function is used to prepare to retrieve locations
    @objc func prepareToRetrieveLocations(sender: NSTimer!) {
        // Check whether this method is called by timer or not
        if sender == nil {
            retrieveLocations(nil)
        } else {
            let nextPageToken = sender.userInfo as! String!
            if nextPageToken == nil {
                retrieveLocations(nil)
            } else {
                retrieveLocations(nextPageToken)
            }
        }
    }
    
    // Actual working function to perform http request
    func retrieveLocations(pageToken: String!) {
        // Based on page token to determine whether we are retrieving following results or perform the search for the first time
        var requestUrl: NSURL
        var isRetrievingNextPage = false
        if pageToken == nil {
            requestUrl = getGooglePlacesSearchURLBasedOnCurrentLocation()
            // Register current location to last updated location
            lastUpdateLocation = currentLocation
        } else {
            requestUrl = getGooglePlacesNextPageURL(pageToken)
            isRetrievingNextPage = true
        }
        
        let urlSessionTask = urlSession.dataTaskWithRequest(NSURLRequest(URL: requestUrl), completionHandler: { (data: NSData?, response: NSURLResponse?, error: NSError?) in
            do {
                
                // Process the data returned by request
                let surroundingsJson = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! JSON
                let response = GooglePlacesAPIResponse(json: surroundingsJson)
                // If we are retrieving next page, do not replace the content of surroundings
                if !isRetrievingNextPage {
                    self.surroundings.removeAll()
                    self.placesHaveBeenSorted = false
                    self.surroundings.appendContentsOf(response!.results!)
                } else {
                    self.surroundings.appendContentsOf(response!.results!)
                }
                
                // Processing the next page token
                if response!.nextPageToken != nil {
                    self.addUpdateSurroundingsTimer(response!.nextPageToken!)
                } else {
                    self.convertSearchResultsToPlaces()
                }
            }
            catch {
                // The error occured, we have to remove everything from surrdounding list
                self.surroundings.removeAll()
            }
        })
        urlSessionTask.resume()
    }
    
    // Convert surroundings feedback to places
    func convertSearchResultsToPlaces() {
        sortedPlacesForSurroundingsAndDetail.removeAll()
        sortedPlacesForNavigation.removeAll()
        // Covert the surroundings to place
        for location in surroundings {
            //print("location name:\(location.name!), category:\(location.types!)")
            if location.geometry != nil && location.geometry!.location != nil {
                var type: PlaceType! = nil
                for typeReturned in location.types! {
                    let placeType = PlaceType(rawValue: typeReturned)
                    if placeType != nil {
                        type = placeType!
                        break
                    }
                }
                
                if type == nil {
                    continue
                }
                let category = SurroundingsInformationProvider.placeTypeToCategory(type)
                if category == nil {
                    continue
                }
                let placeLocation = CLLocation(latitude: location.geometry!.location!.lat!, longitude: location.geometry!.location!.lng!)
                
                let newPlace = Place(aName: location.name!, aCategory: category, aType: type, aRadius: nil, aLocation: placeLocation, aRoute: nil, aDetail: "\(location.name!) is a \(type).")
                sortedPlacesForSurroundingsAndDetail.append(newPlace)
                sortedPlacesForNavigation.append(newPlace)
                
            }
        }
        
        // Add local stored places to the list
        sortedPlacesForNavigation.appendContentsOf(localStoredPlaces!)
        sortSurroundingsByDistanceToCurrentLocation()
    }
    
    // Sort the locations by the distance to user's location
    func sortSurroundingsByDistanceToCurrentLocation() {
        
        // Perform the sort
        sortedPlacesForNavigation = sortedPlacesForNavigation.sort({ (place1: Place, place2: Place) -> Bool in
            let distanceToPlace1 = place1.location!.distanceFromLocation(currentLocation!)
            let distanceToPlace2 = place2.location!.distanceFromLocation(currentLocation!)
            return distanceToPlace1 <= distanceToPlace2
        })
        
        sortedPlacesForSurroundingsAndDetail = sortedPlacesForSurroundingsAndDetail.sort({ (place1: Place, place2: Place) -> Bool in
            let distanceToPlace1 = place1.location!.distanceFromLocation(currentLocation!)
            let distanceToPlace2 = place2.location!.distanceFromLocation(currentLocation!)
            return distanceToPlace1 <= distanceToPlace2
        })
        
        // Set the flag
        placesHaveBeenSorted = true
    }
    
    // Add a timer to retrieve the following search results provided by google
    func addUpdateSurroundingsTimer(nextPageToken: String) {
        // If next page token is available, we want to retrieve the google api every 2 seconds until all the results have been returned
        let timer = NSTimer(timeInterval: 2, target: self, selector: #selector(prepareToRetrieveLocations), userInfo: nextPageToken, repeats: false)
        NSRunLoop.mainRunLoop().addTimer(timer, forMode: NSRunLoopCommonModes)
    }
    
    // Check whether we need to update surroundings or not
    @objc func checkLocationUpdates() {
        if lastUpdateLocation != nil && currentLocation != nil && lastUpdateLocation!.distanceFromLocation(currentLocation!) > 50 {
            prepareToRetrieveLocations(nil)
        }
    }
    
    // MARK: - Delegate Functions: Location Service Delgate Functions
    func locationUpdated(location: CLLocation) {
        if currentLocation == nil {
            currentLocation = location
            prepareToRetrieveLocations(nil)
        } else {
            currentLocation = location
        }
    }
    
    func headingUpdated(heading: CLHeading) {
        currentHeading = heading
    }
    
    // This method is used to convert place type to category
    static func placeTypeToCategory(type: PlaceType) -> PlaceCategory! {
        switch type {
        case .park: return .Landmarks
        case .amusement_park: return .Landmarks
        case .bakery: return .Food
        case .cafe: return .Food
        case .store: return .Store
        case .clothing_store: return .Store
        case .convenience_store: return .Store
        case .liquor_store: return .Store
        case .department_store: return .Store
        case .grocery_or_supermarket: return .Store
        case .food: return .Food
        case .restaurant: return .Food
        case .gym: return .Health
        case .doctor: return .Medical
        case .hospital: return .Medical
        case .physiotherapist: return .Medical
        case .pharmacy: return .Medical
        case .atm: return .Finance
        case .bank: return .Finance
        case .bus_station: return .Transport
        case .subway_station: return .Transport
        case .train_station: return .Transport
        case .transit_station: return .Transport
        case .church: return .Religion
        case .casino: return .Entertainment
        case .bar: return .Entertainment
        case .night_club: return .Entertainment
        case .toilet: return .PublicInfrustructure
        case .waterfountain: return .PublicInfrustructure
        case .rubbish_bin: return .PublicInfrustructure
        case .bbq_machine: return .PublicInfrustructure
        case .bench: return .PublicInfrustructure
        case .shower: return .PublicInfrustructure
        case .sculpture: return .PublicInfrustructure
        case .stair: return .DangerousLocation
        case .edge: return .DangerousLocation
        case .wall: return .DangerousLocation
        case .electronics_store: return .Store
        case .dentist: return .Medical
        default: return nil
        }
    }
    
    // This method is used to convert category to place type
    static func placeCategoryToType(category: PlaceCategory) -> [PlaceType] {
        switch category {
        case .Landmarks : return [PlaceType.park, PlaceType.amusement_park]
        case .Food : return [PlaceType.bakery, PlaceType.cafe, PlaceType.restaurant, PlaceType.food]
        case .Medical : return [PlaceType.doctor, PlaceType.hospital, PlaceType.physiotherapist, PlaceType.pharmacy, PlaceType.dentist]
        case .Finance : return [PlaceType.atm, PlaceType.bank]
        case .Transport : return [PlaceType.bus_station, PlaceType.subway_station, PlaceType.train_station, PlaceType.transit_station]
        case .Religion : return [PlaceType.church]
        case .Health: return [PlaceType.gym]
        case .Store: return [PlaceType.store, PlaceType.clothing_store, PlaceType.liquor_store, PlaceType.convenience_store, PlaceType.department_store, PlaceType.grocery_or_supermarket, PlaceType.electronics_store]
        case .Entertainment : return [PlaceType.casino, PlaceType.bar, PlaceType.night_club]
        case .PublicInfrustructure: return [PlaceType.toilet, PlaceType.waterfountain, PlaceType.rubbish_bin, PlaceType.bbq_machine, PlaceType.bench, PlaceType.shower, PlaceType.sculpture]
        case .DangerousLocation: return [PlaceType.stair, PlaceType.edge, PlaceType.wall]
        }
    }
}

// This enumerate is used to store the category information about the locations
enum PlaceCategory : String {
    case Landmarks = "Landmark"
    case Food = "Food"
    case Medical = "Medical"
    case Finance = "Finance"
    case Transport = "Transport"
    case Religion = "Religion"
    case Entertainment = "Entertainment"
    case PublicInfrustructure = "PublicInfrustructure"
    case DangerousLocation = "DangerousLocation"
    case Health = "Health"
    case Store = "Store"
}

// This enumerate is used to represent the type of the locations
enum PlaceType : String {
    case amusement_park = "amusement_park"
    case atm = "atm"
    case bakery = "bakery"
    case bank = "bank"
    case bar = "bar"
    case bus_station = "bus_station"
    case cafe = "cafe"
    case casino = "casino"
    case cemetery = "cemetery"
    case church = "church"
    case city_hall = "city_hall"
    case clothing_store = "clothing_store"
    case convenience_store = "convenience_store"
    case department_store = "department_store"
    case doctor = "doctor"
    case dentist = "dentist"
    case electronics_store = "electronics_store"
    case food = "food"
    case gym = "gym"
    case hospital = "hospital"
    case insurance_agency = "insurance_agency"
    case library = "library"
    case liquor_store = "liquor_store"
    case museum = "museum"
    case night_club = "night_club"
    case park = "park"
    case pharmacy = "pharmacy"
    case physiotherapist = "physiotherapist"
    case police = "police"
    case post_office = "post_office"
    case restaurant = "restaurant"
    case school = "school"
    case shopping_mall = "shopping_mall"
    case stadium = "stadium"
    case store = "store"
    case subway_station = "subway_station"
    case train_station = "train_station"
    case transit_station = "transit_station"
    case university = "university"
    case grocery_or_supermarket = "grocery_or_supermarket"
    case toilet = "Toilets"
    case waterfountain = "Waterfountain"
    case rubbish_bin = "Rubbish Bin"
    case bbq_machine = "BBQ Machine"
    case bench = "Bench"
    case shower = "Shower"
    case sculpture = "Sculpture"
    case stair = "Stair"
    case edge = "Edge"
    case wall = "Wall"
}
