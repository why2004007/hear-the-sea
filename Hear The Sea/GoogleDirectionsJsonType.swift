//
//  GoogleDirectionsJsonType.swift
//  Hear The Sea
//
//  Created by Daniel Liu on 2/09/2016.
//  Copyright © 2016 Daniel Liu. All rights reserved.
//

import Foundation
import Gloss

struct GeocodedWaypoint: Decodable {
    
    let geocoderStatus : String!
    let placeId : String!
    let types : [String]!
    
    init?(json: JSON){
        geocoderStatus = "geocoder_status" <~~ json
        placeId = "place_id" <~~ json
        types = "types" <~~ json
    }
    
}

struct GeoLocation: Decodable {
    
    let lat : Double!
    let lng : Double!
    
    init?(json: JSON){
        lat = "lat" <~~ json
        lng = "lng" <~~ json
    }
    
}

struct Distance: Decodable {
    
    let text : String!
    let value : Int!
    
    init?(json: JSON){
        text = "text" <~~ json
        value = "value" <~~ json
    }
    
}

struct Step: Decodable {
    
    let distance : Distance!
    let duration : Distance!
    let endLocation : GeoLocation!
    let htmlInstructions : String!
    let maneuver : String!
    let startLocation : GeoLocation!
    let travelMode : String!
    
    init?(json: JSON){
        distance = "distance" <~~ json
        duration = "duration" <~~ json
        endLocation = "end_location" <~~ json
        htmlInstructions = "html_instructions" <~~ json
        maneuver = "maneuver" <~~ json
        startLocation = "start_location" <~~ json
        travelMode = "travel_mode" <~~ json
    }
    
}

struct Leg: Decodable {
    
    let distance : Distance!
    let duration : Distance!
    let endAddress : String!
    let endLocation : GeoLocation!
    let startAddress : String!
    let startLocation : GeoLocation!
    let steps : [Step]!
    let trafficSpeedEntry : [AnyObject]!
    let viaWaypoint : [AnyObject]!
    
    init?(json: JSON){
        distance = "distance" <~~ json
        duration = "duration" <~~ json
        endAddress = "end_address" <~~ json
        endLocation = "end_location" <~~ json
        startAddress = "start_address" <~~ json
        startLocation = "start_location" <~~ json
        steps = "steps" <~~ json
        trafficSpeedEntry = "traffic_speed_entry" <~~ json
        viaWaypoint = "via_waypoint" <~~ json
    }
    
}

struct Route: Decodable {
    
    let legs : [Leg]!
    
    init?(json: JSON){
        legs = "legs" <~~ json
    }
    
}

struct GoogleDirectionAPIResponse: Decodable {
    
    let geocodedWaypoints : [GeocodedWaypoint]!
    let routes : [Route]!
    let status : String!
    
    init?(json: JSON){
        geocodedWaypoints = "geocoded_waypoints" <~~ json
        routes = "routes" <~~ json
        status = "status" <~~ json
    }
    
}