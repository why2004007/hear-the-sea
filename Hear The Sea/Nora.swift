//
//  JellyfishService.swift
//  Intermmediate class between command dispatch and speech recognition
//  Hear The Sea
//
//  Created by Daniel Liu on 1/09/2016.
//  Copyright © 2016 Daniel Liu. All rights reserved.
//

import UIKit

class Nora {
    
    var narrationService:NarrationService
    var alarmService:AlarmService
    var weatherInformationProvider:WeatherInformationProvider
    var surroundingsInformationProvider:SurroundingsInformationProvider
    var navigationService: NavigationService
    
    init(aNarrationService: NarrationService, aAlarmService: AlarmService, aWeatherInformationProvider: WeatherInformationProvider, aSurroundingsInformationProvider: SurroundingsInformationProvider, aNavigationService: NavigationService) {
        narrationService = aNarrationService
        alarmService = aAlarmService
        weatherInformationProvider = aWeatherInformationProvider
        surroundingsInformationProvider = aSurroundingsInformationProvider
        navigationService = aNavigationService
    }
    
    func commandReceived(command: String) {
        print(command)
        switch command {
        case "Who are you" : introduceMyself()
        case "What's your name": introduceMyself()
        case "Hi": introduceMyself()
        case "Weather": currentWeather()
        case "Current weather": currentWeather()
        case "Current temperature": currentTemperature()
        case "UV index": currentUVIndex()
        case "Ultraviolet index": currentUVIndex()
        case "Ultraviolet": currentUVIndex()
        case "UV": currentUVIndex()
        case "Windspeed": currentWindSpeed()
        case "Wind speed": currentWindSpeed()
        case "Current wind speed": currentWindSpeed()
        case "Current windspeed": currentWindSpeed()
        case "Weather condition": currentWeatherCondition()
        case "Weather conditions": currentWeatherCondition()
        case "Tell me my surroundings": surroundings()
        case "Surroundings": surroundings()
        case "Details": details()
        case "Disable help of navigation": disableNavigationHelp()
        case "Disable help off navigation": disableNavigationHelp()
        case "Disable help of a navigation": disableNavigationHelp()
        case "Disable help of the navigation": disableNavigationHelp()
        case "Disable help off the navigation": disableNavigationHelp()
        case "Enable help of navigation": enableNavigationHelp()
        case "Enable help off navigation": enableNavigationHelp()
        case "Enable help of a navigation": enableNavigationHelp()
        case "Enable help of the navigation": enableNavigationHelp()
        case "Start navigation": startNavigation()
        case "Navigation": startNavigation()
        case "Navigate by category": navigateByCategory()
        case "Start navigation by category": navigateByCategory()
        case "Help": narrateHelp()
        case "Navigation help": navigationHelp()
        case "Surroundings help": surroundingsHelp()
        case "Surrounding help": surroundingsHelp()
        case "Weather help": weatherHelp()
        case "Nora help": noraHelp()
        case "Command": noraHelp()
        case "Commands": noraHelp()
        case "Nora": noraHelp()
        case "I love you": loveYou()
        case "Thank you": greetings()
        case "Thank you very much": greetings()
        case "Thanks": greetings()
        default: unrecognisedCommand()
        }
    }
    
    func surroundings() {
        surroundingsInformationProvider.narrateSurroundings()
    }
    
    func details() {
        surroundingsInformationProvider.narrateDetails()
    }
    
    func introduceMyself() {
        narrationService.normalMessage("Hi, I'm nora.")
    }
    
    func currentWeather() {
        weatherInformationProvider.narrateWeather(WeatherInformationCategory.OverallWeather)
    }
    
    func currentUVIndex() {
        weatherInformationProvider.narrateWeather(WeatherInformationCategory.UVIndex)
    }
    
    func currentWindSpeed() {
        weatherInformationProvider.narrateWeather(WeatherInformationCategory.WindSpeed)
    }
    
    func currentWeatherCondition() {
        weatherInformationProvider.narrateWeather(WeatherInformationCategory.WeatherConditions)
    }
    
    func unrecognisedCommand() {
        narrationService.normalMessage("I can not understand what you said. Please try again.")
    }
    
    func currentTemperature() {
        weatherInformationProvider.narrateWeather(WeatherInformationCategory.CurrentTemperature)
    }
    
    func startNavigation() {
        navigationService.prepareToNavigate()
    }
    
    func navigateByCategory() {
        narrationService.narrateWithCallback(CallbackSender.Navigation, string: "Attention, you still need to use button to select destination.", callbackInterval: 0.5) {
            self.startNavigationByCategory()
        }
    }
    
    func startNavigationByCategory() {
        navigationService.destinationSelectingPhase = DestinationSelectionPhase.Category
        navigationService.prepareToNavigate()
    }
    
    func disableNavigationHelp() {
        if navigationService.needNavigationHelp {
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            appDelegate.settings!.setValue(false, forKey: "NavigationHelpEnabled")
            appDelegate.saveSettings()
            navigationService.needNavigationHelp = false
            narrationService.normalMessage("Navigation help is disabled. You can enable it by saying enable help of navigation")
        } else {
            narrationService.normalMessage("Navigation help is already disabled.")
        }
    }
    
    func enableNavigationHelp() {
        if !navigationService.needNavigationHelp {
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            appDelegate.settings!.setValue(true, forKey: "NavigationHelpEnabled")
            appDelegate.saveSettings()
            navigationService.needNavigationHelp = true
            narrationService.normalMessage("Navigation help is enabled. You can disable it by saying disable help of navigation")
        } else {
            narrationService.normalMessage("Navigation help is already enabled.")
        }
        
    }
    
    func loveYou() {
        narrationService.normalMessage("I love you too.")
    }
    
    func greetings() {
        narrationService.normalMessage("You are welcome, it's my pleasure.")
    }
    
    func narrateHelp() {
        narrationService.normalMessage("If you want help about the Navigation function please press bottom left button and tell NORA Navigation help, if you want help about the Surroundings function then please press the bottom left button and tell NORA “Surrounding help”, if you want help about the weather function please press bottom left button and tell NORA Weather help.")
    }
    
    func navigationHelp() {
        narrationService.normalMessage("Navigation helps you to get the locations where you wish to go. You can press Navigation button to know five nearest surroundings. There will be a pause between each of surroundings, if you have a target location, press Navigation button again. This will redirect you to apple map. When the navigation is finished, you can press the very left-top corner of the screen to switch back to this application. If you want to disable the help of navigation you can say “disable help of navigation” to Nora.")
    }
    
    func surroundingsHelp() {
        narrationService.normalMessage("The Surroundings information provided by this application would include the five closest objects to you. The surroundings button facilitates two functions which are the objects list and the details of the objects in the list. To know the surrounding information please press the top right button.If NORA asks you Do you want to know more about surroundings ? please press the Surroundings button again within 3 seconds to hear the next five objects. You may also know the surrounding information by pressing the bottom left button and telling NORA Surroundings. If you want to know about the details of the surroundings, please hold the surroundings button for about a second to begin details mode. There will be a pause after each object, if you are interested in it, please hold the same button again for a second to hear its details. You may also know the details by pressing the bottom left button and telling NORA Details.")
    }
    
    func weatherHelp() {
        narrationService.normalMessage("The Weather information provided by this application would include the current weather condition and the weather condition of the day along with the current temperature, the current wind speed and the current ultraviolet level. To know the weather information please press the top right button. You may also know the complete weather information by pressing the bottom left button and telling NORA Weather, or know current temperature by telling NORA current temperature, know wind speed by telling NORA wind speed, know ultraviolet level by telling NORA uv, know the current weather condition by telling NORA weather condition")
    }
    
    func noraHelp() {
        narrationService.normalMessage("The following commands are the commands that I can understand: For the weather: Current weather, Current temperature, UV Index, Ultraviolet Index, UV, windspeed, current windspeed. For the surroundings: surroundings, tell me surorundings, surroundings, detail. For the navigation: navigation, start navigation, enable help of navigation, disable help of navigation. For me: I can understand some basic greetings, such as I love you. Hi. and what's your name.")
    }
}
