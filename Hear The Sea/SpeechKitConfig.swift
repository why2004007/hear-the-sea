//
//  SpeechKitConfig.swift
//  Hear The Sea
//
//  Created by Daniel Liu on 1/09/2016.
//  Copyright © 2016 Daniel Liu. All rights reserved.
//

import Foundation

var SKSAppKey = "0f070af560a66247308b65aa76da72320bd0f98a499d795f915dbf8aeab9b2fac0bcef2062516893ddcd3d53a7b085b2807f3cc91cb628ad13e737fda1eaf761"
var SKSAppId = "NMDPPRODUCTION_Daniel_Liu_Hear_The_Sea_20160831202602"
var SKSServerHost = "jwp.nmdp.nuancemobility.net"
var SKSServerPort = "443"
var SKSLanguage = "eng-USA"
var SKSServerUrl = String(format: "nmsps://%@@%@:%@", SKSAppId, SKSServerHost, SKSServerPort)